const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
  // The data will be received from the registration form
  // When the user logs in, a token will be created with user's information
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };
  // Generate a JSON web token using the jwt's sign method
  // Generates the token using the form data, and the secret code with no additional options provided
  return jwt.sign(data, secret, {});
};
