const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
  return User.find({ email: reqBody.email }).then((result) => {
    if (result.length > 0) {
      return true;
    } else {
      return false;
    }
  });
};

// register
module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password, 10),
    /* 
    Syntax:
        bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)
    */
  });
  return newUser.save().then((user, err) => {
    if (err) {
      return false;
    } else {
      return true;
    }
  });
};

// login
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result == null) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );
      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        return false;
      }
    }
  });
};

// Activity S33

// get profile by ID
module.exports.getProfile = (userId) => {
  return User.findById(userId).then((result, err) => {
    if (err) {
      console.log(err);
      return false;
    } else {
      result.password = "";
      return result;
    }
  });
};
