const express = require("express");
const router = express.Router();
const userController = require("../controllers/User");

router.post("/checkEmail", (req, res) => {
  userController
    .checkEmailExists(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Activity S33

// route in getting profile
router.post("/details", (req, res) => {
  userController
    .getProfile(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
